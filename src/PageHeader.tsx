import { Container, Dropdown, DropdownButton, Navbar, Nav } from 'react-bootstrap';
import { GeneralInfo, CarInfo } from './Interfaces';
import { sort } from './Parsing';
import logo from './logo.png';

interface Props {
  generalInfo: GeneralInfo;
  carInfoArray: CarInfo[];
  setCarInfoArray: React.Dispatch<React.SetStateAction<CarInfo[]>>;
}

export default function PageHeader({ generalInfo, carInfoArray, setCarInfoArray }: Props) {

  function sortCarOrder(sortBy: string, sortMultiplier: number) {
    let sortArray = carInfoArray;
    sortArray = sort(sortArray, sortBy, sortMultiplier);
    setCarInfoArray(sortArray);
  }

  return (
    <Navbar bg="light" sticky="top">
      <Container>
        <Nav>
          <img
            className="logo"
            style={{ paddingLeft: '2.5rem', maxWidth: '10rem' }}
            src={logo}
            alt="Logo"
          />
        </Nav>
        <Nav>
          <div>
            <strong>Pickup:</strong>
            {' '}
            {generalInfo.pickUpDate}
            {' '}
            @
            {' '}
            {generalInfo.pickUpLocation}
            <br />
            <strong>Return:</strong>
            {' '}
            {generalInfo.returnDate}
            {' '}
            @
            {' '}
            {generalInfo.returnLocation}
            <br />
          </div>
        </Nav>
        <Nav>
          <DropdownButton
            title="Sort"
            variant="secondary"
            style={{ paddingRight: '2.5rem' }}
          >
            <Dropdown.Item
              eventKey="priceA"
              onClick={() => sortCarOrder('price', 1)}
            >
              Price (Ascending)
            </Dropdown.Item>
            <Dropdown.Item
              eventKey="priceD"
              onClick={() => sortCarOrder('price', -1)}
            >
              Price (Descending)
            </Dropdown.Item>
            <Dropdown.Item
              eventKey="doorsA"
              onClick={() => sortCarOrder('doors', 1)}
            >
              Doors (Ascending)
            </Dropdown.Item>
            <Dropdown.Item
              eventKey="doorsD"
              onClick={() => sortCarOrder('doors', -1)}
            >
              Doors (Descending)
            </Dropdown.Item>
            <Dropdown.Item
              eventKey="baggageA"
              onClick={() => sortCarOrder('baggage', 1)}
            >
              Baggage (Ascending)
            </Dropdown.Item>
            <Dropdown.Item
              eventKey="baggageD"
              onClick={() => sortCarOrder('baggage', -1)}
            >
              Baggage (Descending)
            </Dropdown.Item>
            <Dropdown.Item
              eventKey="vendorA"
              onClick={() => sortCarOrder('vendor', 1)}
            >
              Vendor Name (A → Z)
            </Dropdown.Item>
            <Dropdown.Item
              eventKey="vendorD"
              onClick={() => sortCarOrder('vendor', -1)}
            >
              Vendor Name (Z → A)
            </Dropdown.Item>
          </DropdownButton>
        </Nav>
      </Container>
    </Navbar>
  );
}
