import '@testing-library/jest-dom';
import responseJson from './cars.json'
import { getGeneralInfo, getCarInfoArray } from './Parsing';

test('car info is parsed correctly', async () => {

    const carInfoArray = getCarInfoArray(responseJson);
    expect(carInfoArray[0].vendor).toBe('Hertz')
    expect(carInfoArray[1].model).toBe('Ford Focus or similar')
    expect(carInfoArray[2].price).toBe(752)
    expect(carInfoArray[3].currency).toBe('CAD')
    expect(carInfoArray[4].baggage).toBe(4)
    expect(carInfoArray[5].driveType).toBe('Unspecified')
    expect(carInfoArray[6].doors).toBe(5)
    expect(carInfoArray[7].fuel).toBe('Petrol')
    expect(carInfoArray[8].passengers).toBe(5)
    expect(carInfoArray[9].transmission).toBe('Automatic')
    expect(carInfoArray[0].pictureURL).toBe('https://ctimg-fleet.cartrawler.com/kia/rio/primary.png?auto=format&w=600')
    expect(carInfoArray[1].aircon).toBe('Yes')

})

test('general info is parsed correctly', async () => {

    const generalInfo = getGeneralInfo(responseJson);
    expect(generalInfo.pickUpDate).toBe('Sun, 22 Mar 2020')
    expect(generalInfo.returnDate).toBe('Mon, 06 Apr 2020')
    expect(generalInfo.pickUpLocation).toBe('Las Vegas - Airport')
    expect(generalInfo.returnLocation).toBe('Las Vegas - Airport')

})