import { useState } from 'react';
import { Container, Row, Col, Button, } from 'react-bootstrap';
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import CarDetails from './CarDetails';
import CarModal from './CarModal';
import { CarInfo } from './Interfaces';
import './App.css';

interface Props {
  carInfoArray: CarInfo[];
}

export default function CarList({ carInfoArray: carInfo }: Props) {
  const [modalShow, setModalShow] = useState(false);
  const [selectedCarInfo, setSelectedCarInfo] = useState({} as CarInfo);

  function setModal(modalCarInfo: CarInfo) {
    setModalShow(true);
    setSelectedCarInfo(modalCarInfo);
  }

  return (
    <>
      <Container>
        <Row>
          {carInfo.map((vehicleData, i) => (
            <Col key={i}>
              <Box p={5}>
                <Paper>
                  <Box style={{ padding: '2rem' }}>
                    <Row>
                      <Col>
                        <Row>
                          <Col>
                            <h2>{vehicleData.model}</h2>
                            <h4>{vehicleData.vendor}</h4>
                            <br />
                          </Col>
                        </Row>
                        <CarDetails carInfoProp={vehicleData} />
                      </Col>
                      <Col>
                        <img
                          src={vehicleData.pictureURL}
                          className="image"
                          style={{ maxWidth: '24rem' }}
                          alt="Car"
                        />
                        <Button
                          className="button"
                          variant="success"
                          onClick={() => setModal(vehicleData)}
                        >
                          More Info
                        </Button>
                      </Col>
                    </Row>
                    <Row />
                  </Box>
                </Paper>
              </Box>
            </Col>
          ))}
        </Row>
      </Container>
      <CarModal
        setModalShow={setModalShow}
        carInfo={selectedCarInfo}
        show={modalShow}
      />
    </>
  );
}
