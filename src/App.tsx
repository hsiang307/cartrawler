import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import { useState, useEffect } from 'react';
import PageHeader from './PageHeader';
import CarList from './CarList';
import { getGeneralInfo, getCarInfoArray } from './Parsing';
import { CarInfo, GeneralInfo } from './Interfaces';
import { Spinner, Button, Modal } from 'react-bootstrap';

export default function App() {
  type FetchState = 'fetching' | 'fetched' | 'error';
  const [fetchState, setFetchState] = useState('fetching' as FetchState);
  const [carInfoArray, setCarInfoArray] = useState([{} as CarInfo]);
  const [generalInfo, setGeneralInfo] = useState({} as GeneralInfo);

  async function fetchJSON() {
    setFetchState('fetching')
    try {
      let response = await fetch('https://gentle-castle-29050.herokuapp.com/http://www.cartrawler.com/ctabe/cars.json')
      if (response.ok) {
        let responseJson = await response.json();
        setCarInfoArray(getCarInfoArray(responseJson));
        setGeneralInfo(getGeneralInfo(responseJson));
        setTimeout(() => setFetchState('fetched'), 2000);
      } else {
        setTimeout(() => setFetchState('error'), 2000);
        throw new Error('Response Error');
      }
    } catch (error) {
      setTimeout(() => setFetchState('error'), 2000);
    }
  }

  useEffect(() => {
    fetchJSON();
  }, []);

  switch (fetchState) {
    case 'fetching':
      return (
        <div className="centered">
          {<Spinner className="spinner" animation="border" variant="success" />}
        </div>
      )
    case 'fetched':
      return (
        <>
          <PageHeader generalInfo={generalInfo} carInfoArray={carInfoArray} setCarInfoArray={setCarInfoArray} />
          <CarList carInfoArray={carInfoArray} />
        </>
      );
    case 'error':
      return (
        <Modal show={true}>
          <Modal.Header>
            <Modal.Title>Error</Modal.Title>
          </Modal.Header>
          <Modal.Body>Failed to load</Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={() => fetchJSON()}>
              Retry
            </Button>
          </Modal.Footer>
        </Modal>
      )
  }
}
