import { CarInfo, GeneralInfo } from './Interfaces';

export const sort = (
  array: CarInfo[],
  sortBy: string,
  sortMultiplier: number,
) => {
  const carInfoSortArray: any = [...array];
  if (sortBy === 'vendor') {
    carInfoSortArray.sort((
      a: CarInfo,
      b: CarInfo,
    ) => {
      if (b.vendor > a.vendor) return sortMultiplier * -1;
      if (b.vendor < a.vendor) return sortMultiplier * 1;
      return 0;
    });
  } else {
    carInfoSortArray.sort((
      a: { [x: string]: string },
      b: { [x: string]: string },
    ) => {
      if (parseInt(a[sortBy], 10) < parseInt(b[sortBy], 10)) {
        return sortMultiplier * -1;
      }
      if (parseInt(a[sortBy], 10) > parseInt(b[sortBy], 10)) {
        return sortMultiplier * 1;
      }
      return 0;
    });
  }
  return carInfoSortArray;
};

export const getGeneralInfo = (
  data: { VehAvailRSCore: { VehRentalCore: any } }[],
) => {
  const generalData = data[0].VehAvailRSCore.VehRentalCore;
  const generalInfoObject = {} as GeneralInfo;
  generalInfoObject.pickUpDate = new Date(generalData['@PickUpDateTime'])
    .toUTCString()
    .split(' ')
    .slice(0, 4)
    .join(' ');
  generalInfoObject.returnDate = new Date(generalData['@ReturnDateTime'])
    .toUTCString()
    .split(' ')
    .slice(0, 4)
    .join(' ');
  generalInfoObject.pickUpLocation = generalData.PickUpLocation['@Name'];
  generalInfoObject.returnLocation = generalData.ReturnLocation['@Name'];
  return generalInfoObject;
};

function capitalize(s: string) {
  return s[0].toUpperCase() + s.slice(1).toLowerCase();
}

export const getCarInfoArray = (
  data: { VehAvailRSCore: { VehVendorAvails: any } }[],
) => {
  const carData = data[0].VehAvailRSCore.VehVendorAvails;
  const carInfoArray = [];
  for (let i = 0; i < carData.length; i += 1) {
    for (let j = 0; j < carData[i].VehAvails.length; j += 1) {
      const carInfoObject = {} as CarInfo;
      carInfoObject.vendor = capitalize(carData[i].Vendor['@Name']);
      carInfoObject.model = carData[i].VehAvails[j].Vehicle.VehMakeModel['@Name'];
      carInfoObject.price = parseInt(
        carData[i].VehAvails[j].TotalCharge['@EstimatedTotalAmount'],
        10,
      );
      carInfoObject.currency = carData[i].VehAvails[j].TotalCharge['@CurrencyCode'];
      carInfoObject.baggage = parseInt(
        carData[i].VehAvails[j].Vehicle['@BaggageQuantity'],
        10,
      );
      carInfoObject.driveType = carData[i].VehAvails[j].Vehicle['@DriveType'];
      carInfoObject.doors = parseInt(
        carData[i].VehAvails[j].Vehicle['@DoorCount'],
        10,
      );
      carInfoObject.fuel = carData[i].VehAvails[j].Vehicle['@FuelType'];
      carInfoObject.passengers = parseInt(
        carData[i].VehAvails[j].Vehicle['@PassengerQuantity'],
        10,
      );
      carInfoObject.transmission = carData[i].VehAvails[j].Vehicle['@TransmissionType'];
      carInfoObject.pictureURL = carData[i].VehAvails[j].Vehicle.PictureURL;
      if (carData[i].VehAvails[j].Vehicle['@AirConditionInd'] === 'true') {
        carInfoObject.aircon = 'Yes';
      } else {
        carInfoObject.aircon = 'No';
      }
      carInfoArray.push(carInfoObject);
    }
  }
  const sortedArray = sort(carInfoArray, 'price', 1);
  return sortedArray;
};
