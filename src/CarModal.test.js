import { render, screen } from "@testing-library/react";
import '@testing-library/jest-dom';
import CarModal from "./CarModal";
import responseJson from './cars.json'
import { getCarInfoArray } from './Parsing';


test('both close modal buttons are enabled', () => {

  const carInfo = getCarInfoArray(responseJson);
  let setModalShow = () => {
    return true
  }

  render(<CarModal
    setModalShow={setModalShow}
    carInfo={carInfo[0]}
    show={true}
  />)

  expect(screen.getAllByRole('button')[0]).toBeEnabled();
  expect(screen.getAllByRole('button')[1]).toBeEnabled();

})