import {
  Container, Row, Col, Modal, Button,
} from 'react-bootstrap';
import CarDetails from './CarDetails';
import { CarInfo } from './Interfaces';

interface Props {
  carInfo: CarInfo;
  show: boolean;
  setModalShow: (showModal: boolean) => void;
}

export default function CarModal({
  carInfo: currentVehicle,
  show,
  setModalShow,
}: Props) {
  return (
    <Modal show={show} onHide={() => setModalShow(false)}>
      <Modal.Header closeButton>
        <Modal.Title>
          {currentVehicle.model}
          <br />
          {currentVehicle.vendor}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <img
          src={currentVehicle.pictureURL}
          className="image"
          style={{ maxWidth: '24rem' }}
          alt="Car"
        />
      </Modal.Body>
      <Modal.Footer>
        <Container>
          <CarDetails carInfoProp={currentVehicle} />
          <br />
          <Row>
            <Col>
              <Button
                data-testid="modal-close"
                className="button"
                variant="secondary"
                onClick={() => setModalShow(false)}
              >
                Close
              </Button>
            </Col>
          </Row>
        </Container>
      </Modal.Footer>
    </Modal>
  );
}
