import { Row, Col } from 'react-bootstrap';
import { CarInfo } from './Interfaces';

interface Props {
  carInfoProp: CarInfo;
}

export default function CarDetails({ carInfoProp: carInfo }: Props) {
  return (
    <Row>
      <Col>
        <div>
          <strong>Estimated Price:</strong>
          {' '}
          {carInfo.price}
          {' '}
          {carInfo.currency}
        </div>
        <div>
          <strong>Air Conditioning:</strong>
          {' '}
          {carInfo.aircon}
        </div>
        <div>
          <strong>Baggage Quantity:</strong>
          {' '}
          {carInfo.baggage}
        </div>
        <div>
          <strong>Drive Type:</strong>
          {' '}
          {carInfo.driveType}
        </div>
      </Col>
      <Col>
        <div>
          <strong>Doors:</strong>
          {' '}
          {carInfo.doors}
        </div>
        <div>
          <strong>Fuel Type:</strong>
          {' '}
          {carInfo.fuel}
        </div>
        <div>
          <strong>Passengers:</strong>
          {' '}
          {carInfo.passengers}
        </div>
        <div>
          <strong>Transmission:</strong>
          {' '}
          {carInfo.transmission}
        </div>
      </Col>
    </Row>
  );
}
