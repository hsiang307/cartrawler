export interface CarInfo {
  price: number;
  passengers: number;
  baggage: number;
  doors: number;
  vendor: string;
  model: string;
  currency: string;
  driveType: string;
  fuel: string;
  transmission: string;
  pictureURL: string;
  aircon: string;
}

export interface GeneralInfo {
  pickUpDate: string;
  returnDate: string;
  pickUpLocation: string;
  returnLocation: string;
}
