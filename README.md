## CarTrawler Test App

This is a sample project which sends an HTTP GET request to the CarTrawler API endpoint. The response is parsed and information about each car is displayed. The sort order of the cars can be changed using a drop-down menu.

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the JEST tests. The tests check that the data is parsed correctly, and that the close modal buttons are enabled.

### `npx test`

Launches the Cypress tests. The tests simulate the sort method being changed to 'Price (Descending)' and checks that the data updates accordingly.