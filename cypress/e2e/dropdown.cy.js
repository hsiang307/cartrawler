/* eslint-disable no-undef */
Cypress.config('defaultCommandTimeout', 15000);
describe('dropdown', () => {
    it('user can click dropdown', () => {
        cy.visit('http://localhost:3000/')
        cy.findByRole('button', {
            name: /sort/i
        }).click()
        cy.findByRole('button', {
            name: /price \(descending\)/i
        }).click()
        cy.findAllByRole('button', { name: /more info/i }).then(result => {
            const resultArray = Array.from(result);
            resultArray[0].click();
            cy.findByRole('dialog').contains(/1118 cad/i);
        })

    })
})